package com.example.forestrysafe.model

import com.google.gson.annotations.SerializedName;

data class Feeder(
    @SerializedName("id")
    val feederId: Int,
    val completed: Boolean,
    val status: Boolean,
    val type: String,
    val forestry: Int

)