package com.example.forestrysafe.api

import com.example.forestrysafe.model.Feeder
import retrofit2.Response
import retrofit2.http.GET

interface SimpleApi {

    @GET("api/feeders/1")
    suspend fun getFeeder(): Response<Feeder>
}