package com.example.forestrysafe.repository

import com.example.forestrysafe.api.RetrofitInstance
import com.example.forestrysafe.model.Feeder
import retrofit2.Response

class Repository {

    suspend fun getFeeder(): Response<Feeder> {
        return RetrofitInstance.api.getFeeder()
    }
}