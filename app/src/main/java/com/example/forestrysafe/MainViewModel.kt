package com.example.forestrysafe

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.forestrysafe.model.Feeder
import com.example.forestrysafe.repository.Repository
import kotlinx.coroutines.launch
import retrofit2.Response

class MainViewModel(private val repository: Repository): ViewModel() {

    val myResponce: MutableLiveData<Response<Feeder>> = MutableLiveData()

    fun getFeeder() {
        viewModelScope.launch {
            val response = repository.getFeeder()
            myResponce.value = response
        }
    }

}