package com.example.forestrysafe

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.*
import kotlinx.android.synthetic.main.activity_authorized.*


class AuthorizedActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authorized)

        val recyclerView_feeders = findViewById<RecyclerView>(R.id.recyclerView_feeders)
        recyclerView_feeders.layoutManager = LinearLayoutManager(this)
        recyclerView_feeders.adapter = AuthorizedAdaptor()
    }

}